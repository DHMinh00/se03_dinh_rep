
public class MethodenAuswahlstrukturBeispiele {

	public static void main(String[] args) {
		int erg;
		erg = doit(3, 7, '+'); // 10
		erg = doit(3, 7, '-'); // -4
		erg = doit(3, 7, '?'); // -999
		int zahl = min(12, 9);
		// int erg = max(801, 65);
		// System.out.println("Die kleinere Zahl ist " + zahl);
		System.out.println(erg);
		double minZahl = gibKleinsteZahl(3.5,6.4,2.5);
		System.out.println(minZahl);
	}
	
	public static double gibKleinsteZahl(double z1,double z2, double z3) {
		if (z1 > z2){
			return z2;
		}
		else {
			if(z2 > z3) {
				return z3;
			}
			else {
				return z1;
			}	
		}
	}
		
	public static int min(int zahl1, int zahl2) {
		if (zahl1 < zahl2) {
			return zahl1;
		} else {
			return zahl2;
		}
	}

	public static int max(int zahl1, int zahl2) {
		if (zahl1 < zahl2) {
			return zahl2;
		}
		else {
			return zahl1;
		}
	}

	public static int doit(int zahl1, int zahl2,char op) {
			 
		if (op == '+') {
			return zahl1 + zahl2;
		}
		else {
			if (op == '-') {
			return zahl1 - zahl2;
		}
		else {
			return -999;
			
		}
	}
}
}